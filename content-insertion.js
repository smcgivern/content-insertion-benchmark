var container = document.getElementById('container');

function innerHTML(text) { container.innerHTML = text; }

function jQueryHtml(text) { $(container).html(text); }

function appendChild(text) { container.appendChild($(text)[0]); }

function waitForPopulatedContainer(start, text, rest) {
  var time = performance.now() - start;

  // Minimum time is 100ms, just in case the first frame after doesn't have the changes.
  if (time > 100) {
    console.log('    Done after ' + time.toFixed(2) + 'ms');
    runTests(text, rest);
  } else {
    requestAnimationFrame(function() { waitForPopulatedContainer(start, text, rest); });
  }
}

function runTests(text, fns) {
  if (!fns.length) { return; }

  var fn = fns.shift(),
      now = performance.now();

  container.innerHTML = '';

  console.log('  Starting ' + fn + ':');
  eval(fn + '(text)');
  requestAnimationFrame(function() { waitForPopulatedContainer(now, text, fns); });
}

$('button').each(function(i, button) {
  $.get(button.dataset.url, function(response) {
    $(button).click(function() {
      console.log('Starting ' + button.innerText + ':');

      runTests(JSON.parse(response).html, ['innerHTML', 'jQueryHtml', 'appendChild']);
    });
  });
});
